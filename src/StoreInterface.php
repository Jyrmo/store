<?php

namespace Jyrmo\Store;

interface StoreInterface {
    public function getByKey($key) : Entity;

    public function query($query);

    public function insert(Entity $entity);

    public function update(Entity $entity);

    public function save(Entity $entity);

    public function deleteByKey($key);

    public function delete(Entity $entity);
}
